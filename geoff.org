#+TITLE: geoff.org --- My personal and literate Emacs configuration
#+AUTHOR: Geoffrey T. Wark
#+EMAIL: gw4rk@icloud.com
#+DATE: [2018-07-21 Sat]
#+LANGUAGE:  en


* First Things First

Prevent `customization` from cluttering up init.el

#+BEGIN_SRC emacs-lisp
  (setq custom-file "~/.emacs.d/custom.el")
  (load custom-file :noerror)
#+END_SRC

Save backups/auto-saves to system temp (even if it is dangerous).

#+BEGIN_SRC emacs-lisp
  (setq backup-directory-alist
        `((".*" . ,temporary-file-directory)))
  (setq auto-save-file-name-transforms
        `((".*" ,temporary-file-directory t)))
#+END_SRC

** Sane Defaults

Most of these are taken from: https://github.com/magnars/.emacs.d/blob/master/settings/sane-defaults.el

#+BEGIN_SRC emacs-lisp
  ;; Auto refresh buffers
  (global-auto-revert-mode 1)

  ;; Also auto refresh dired, but be quiet about it
  (setq global-auto-revert-non-file-buffers t)
  (setq auto-revert-verbose nil)

  ;; Show keystrokes in progress
  (setq echo-keystrokes 0.1)

  ;; Move files to trash when deleting
  (setq delete-by-moving-to-trash t)

  ;; Real emacs knights don't use shift to mark things
  (setq shift-select-mode nil)

  ;; Transparently open compressed files
  (auto-compression-mode t)

  ;; Answering just 'y' or 'n' will do
  (defalias 'yes-or-no-p 'y-or-n-p)

  ;; UTF-8 please
  (setq locale-coding-system 'utf-8) ; pretty
  (set-terminal-coding-system 'utf-8) ; pretty
  (set-keyboard-coding-system 'utf-8) ; pretty
  (set-selection-coding-system 'utf-8) ; please
  (prefer-coding-system 'utf-8) ; with sugar on top

  ;; Remove text in active region if inserting text
  (delete-selection-mode 1)

  ;; Always display line and column numbers
  (setq line-number-mode t)
  (setq column-number-mode t)

  ;; Save minibuffer history
  (savehist-mode 1)
  (setq history-length 1000)

  ;; Undo/redo window configuration with C-c <left>/<right>
  (winner-mode 1)

  ;; Never insert tabs
  (set-default 'indent-tabs-mode nil)

  ;; Easily navigate sillycased words
  (global-subword-mode 1)

  ;; Don't break lines for me, please
  (setq-default truncate-lines t)

  ;; Allow recursive minibuffers
  (setq enable-recursive-minibuffers t)

  ;; Don't be so stingy on the memory, we have lots now. It's the distant future.
  (setq gc-cons-threshold 20000000)

  ;; Fontify org-mode code blocks
  ;;(setq org-src-fontify-natively t)

  ;; Sentences do not need double spaces to end. Period.
  (set-default 'sentence-end-double-space nil)

  ;; 80 chars is a good width.
  (set-default 'fill-column 80)

  ;; Add parts of each file's directory to the buffer name if not unique
  (require 'uniquify)
  (setq uniquify-buffer-name-style 'forward)

  (show-paren-mode t)

  ;; Don't beep at me
  (setq visible-bell t)
#+END_SRC

* UI
** Theme

[[http://kippura.org/zenburnpage/][Zenburn]] is pretty easy on the eyes.  I also make the cursor easier to see by making it red.

#+BEGIN_SRC emacs-lisp
  (use-package zenburn-theme
    :ensure t
    :init
    (progn
      (load-theme 'zenburn t)
      (set-cursor-color "red")))
#+END_SRC

Make selection more obvious.

#+BEGIN_SRC emacs-lisp
  (set-face-attribute 'region nil :background "white smoke" :foreground "black")
  (set-face-attribute 'secondary-selection nil :background "white smoke" :foreground "black")
#+END_SRC

Highlight codeblocks in org mode.

#+BEGIN_SRC emacs-lisp
  (set-face-attribute 'org-block nil :background "gray15")
  (set-face-attribute 'org-block-begin-line nil :background "gray15")
  (set-face-attribute 'org-block-end-line nil :background "gray15")
  (set-face-attribute 'org-block-background nil :background "gray15")
#+END_SRC

Italicize comments.

#+BEGIN_SRC emacs-lisp
  (make-face-italic 'font-lock-comment-face)
#+END_SRC

** Font Size

#+BEGIN_SRC emacs-lisp
  (set-face-attribute 'default nil :height 110)
#+END_SRC

* Custom Functions

Rename file and buffer

#+BEGIN_SRC emacs-lisp
  ;; source: http://steve.yegge.googlepages.com/my-dot-emacs-file
  (defun rename-file-and-buffer (new-name)
    "Renames both current buffer and file it's visiting to NEW-NAME."
    (interactive "sNew name: ")
    (let ((name (buffer-name))
          (filename (buffer-file-name)))
      (if (not filename)
          (message "Buffer '%s' is not visiting a file!" name)
        (if (get-buffer new-name)
            (message "A buffer named '%s' already exists!" new-name)
          (progn
            (rename-file filename new-name 1)
            (rename-buffer new-name)
            (set-visited-file-name new-name)
            (set-buffer-modified-p nil))))))
#+END_SRC

** My Functions

Run Twitch streams from within Emacs.

#+BEGIN_SRC emacs-lisp
  (defun my/start-twitch-stream ()
    "Run a Twitch stream from within Emacs via streamlink.

  Asks for a channel name and quality."
    (interactive)
    (let ((channel (read-string "Channel: "))
          (quality (read-string "Quality: ")))
      (start-process "my/start-twitch-stream"
                     "*Twitch Streams*"
                     "streamlink" (concat "twitch.tv/" channel) quality)))
#+END_SRC

* Misc.

Unbind pesky sleep button.

#+BEGIN_SRC emacs-lisp
  (global-unset-key [(control z)])
  (global-unset-key [(control x)(control z)])
#+END_SRC

Stop the cursor from blinking!  So distracting otherwise...

#+BEGIN_SRC emacs-lisp
  (blink-cursor-mode 0)
#+END_SRC

Show when file ends.

#+BEGIN_SRC emacs-lisp
  (setq-default indicate-empty-lines t)
#+END_SRC

* Packages

These are obtained from [[https://melpa.org/#/][MELPA]] unless otherwise stated.

#+BEGIN_SRC emacs-lisp
  ;; we always want to ensure packages get installed
  ;; NOTE: This seems to fail for some cases...
  ;;       Use `:ensure t` for those that do
  (setq use-package-always-ensure t)

  ;; may need to run `package-refresh-contents' first so that all packages will get picked up
  ;;(package-refresh-contents)
#+END_SRC

Keep GNU ELPA key up-to-date.

#+BEGIN_SRC emacs-lisp
  (use-package gnu-elpa-keyring-update)
#+END_SRC

** =auto-package-update=

Automatically update Emacs packages.

#+BEGIN_SRC emacs-lisp
  (use-package auto-package-update
    :config
    (setq auto-package-update-delete-old-versions t)
    (setq auto-package-update-hide-results t)
    (auto-package-update-maybe))
#+END_SRC

** =company-mode=

Modular text completion framework

#+BEGIN_SRC emacs-lisp
  (use-package company
    :init
    (add-hook 'after-init-hook 'global-company-mode))
#+END_SRC

** =git-gutter-fringe=

Fringe version of git-gutter.el

#+BEGIN_SRC emacs-lisp
  (use-package git-gutter-fringe
    :config
    (set-face-foreground 'git-gutter-fr:added    "green")
    (set-face-foreground 'git-gutter-fr:deleted  "hot pink")
    (set-face-foreground 'git-gutter-fr:modified "yellow")

    (global-git-gutter-mode t))
#+END_SRC

** =ido-mode=

and some related packages...

#+BEGIN_SRC emacs-lisp
  (use-package ido
    :init
    (use-package ido-complete-space-or-hyphen)
    (setq ido-everywhere t)
    (ido-mode t)
    (use-package ido-vertical-mode
      :init
      (ido-vertical-mode t)
      (setq ido-vertical-define-keys 'C-n-and-C-p-only))
    (use-package flx-ido
      :init
      (flx-ido-mode 1)
      ;; disable ido faces to see flx highlights
      (setq ido-enable-flex-matching t)
      (setq ido-use-faces nil)))
#+END_SRC

** =impatient-mode=

See the effect of your HTML as you type it.

#+BEGIN_SRC emacs-lisp
  (use-package impatient-mode)
#+END_SRC

** =magit=

A Git porcelain inside Emacs.

#+BEGIN_SRC emacs-lisp
  (use-package magit
    :bind ("C-c g" . magit-status)
    :config
    
    ;; full screen magit-status

    (defadvice magit-status (around magit-fullscreen activate)
      (window-configuration-to-register :magit-fullscreen)
      ad-do-it
      (delete-other-windows))

    (defun magit-quit-session ()
      "Restores the previous window configuration and kills the magit buffer"
      (interactive)
      (kill-buffer)
      (jump-to-register :magit-fullscreen))

    (define-key magit-status-mode-map (kbd "q") 'magit-quit-session))
#+END_SRC

** =nyan-mode=

Nyan Cat shows position in current buffer in mode-line.

#+BEGIN_SRC emacs-lisp
  (use-package nyan-mode
    :config
    (nyan-mode 1))
#+END_SRC

** =page-break-lines=

Display ^L page breaks as tidy horizontal lines

#+BEGIN_SRC emacs-lisp
  (use-package page-break-lines
    :config
    (global-page-break-lines-mode))
#+END_SRC

** =rainbow-delimiters=

Highlight brackets according to their depth

#+BEGIN_SRC emacs-lisp
  (use-package rainbow-delimiters
    :config
    (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))
#+END_SRC

** =simpleclip=

Simplified access to the system clipboard

#+BEGIN_SRC emacs-lisp
  (use-package simpleclip
    :commands (simpleclip-copy simpleclip-cut simpleclip-paste)
    :bind (("C-1" . simpleclip-copy)
           ("C-2" . simpleclip-cut)
           ("C-3" . simpleclip-paste))
    :config
    (simpleclip-mode 1))
#+END_SRC

No longer need the toolbar.

#+BEGIN_SRC emacs-lisp
  (tool-bar-mode -1)
#+END_SRC

** =skewer-mode=

live browser JavaScript, CSS, and HTML interaction

#+BEGIN_SRC emacs-lisp
  (use-package skewer-mode
    :config
    (add-hook 'js2-mode-hook 'skewer-mode)
    (add-hook 'css-mode-hook 'skewer-css-mode)
    (add-hook 'html-mode-hook 'skewer-html-mode)

    ;; set default root folder for httpd server
    (setq httpd-root "~/warkspace"))
#+END_SRC

** =smartparens=

Automatic insertion, wrapping and paredit-like navigation with user defined pairs.

#+BEGIN_SRC emacs-lisp
  (use-package smartparens
    :config
    (require 'smartparens-config)
    (add-hook 'prog-mode-hook #'smartparens-mode))
#+END_SRC

** =smex=

#+BEGIN_SRC emacs-lisp
  (use-package smex
    :bind (("M-x" . smex)
           ("M-X" . smex-major-mode-commands)
           ("C-c C-c M-x" . execute-extended-command))
    :init
    (smex-initialize))
#+END_SRC

** =web-mode=

major mode for editing web templates

#+BEGIN_SRC emacs-lisp
  (use-package web-mode
    :config
    (require 'web-mode)
    (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
    
    (defun my/web-mode-hook ()
      "Hooks for Web mode."
      (setq web-mode-markup-indent-offset 2)
      (setq web-mode-css-indent-offset 2)
      (setq web-mode-code-indent-offset 2)
      ;; this may fix my syntax highlighting issue
      (setq web-mode-enable-element-content-fontification t))

    (add-hook 'web-mode-hook 'my/web-mode-hook))
#+END_SRC

* Org-mode related

Change the ellipsis to be a cornered arrow ([[http://endlessparentheses.com/changing-the-org-mode-ellipsis.html][source]]).

#+BEGIN_SRC emacs-lisp
  (setq org-ellipsis "\u2935")
#+END_SRC

Hide markup elements.

#+BEGIN_SRC emacs-lisp
  (setq org-hide-emphasis-markers t)
#+END_SRC

** =org-bullets=

Show bullets in org-mode as UTF-8 characters

#+BEGIN_SRC emacs-lisp
  (use-package org-bullets
    :ensure t
    :init
    (add-hook 'org-mode-hook 'org-bullets-mode))
#+END_SRC

* Final Steps

Load more private settings.

#+BEGIN_SRC emacs-lisp
  (if (file-exists-p "private.org")
      (org-babel-load-file (concat user-emacs-directory "private.org")))
#+END_SRC

Ensure the config and been successfully loaded by sending a cute ASCII art message (made with http://www.patorjk.com/software/taag/).

#+BEGIN_SRC emacs-lisp
  (message "

         ███▄ ▄███▓ ██▓  ██████   ██████  ▒█████   ███▄    █
        ▓██▒▀█▀ ██▒▓██▒▒██    ▒ ▒██    ▒ ▒██▒  ██▒ ██ ▀█   █
        ▓██    ▓██░▒██▒░ ▓██▄   ░ ▓██▄   ▒██░  ██▒▓██  ▀█ ██▒
        ▒██    ▒██ ░██░  ▒   ██▒  ▒   ██▒▒██   ██░▓██▒  ▐▌██▒
        ▒██▒   ░██▒░██░▒██████▒▒▒██████▒▒░ ████▓▒░▒██░   ▓██░
        ░ ▒░   ░  ░░▓  ▒ ▒▓▒ ▒ ░▒ ▒▓▒ ▒ ░░ ▒░▒░▒░ ░ ▒░   ▒ ▒
        ░  ░      ░ ▒ ░░ ░▒  ░ ░░ ░▒  ░ ░  ░ ▒ ▒░ ░ ░░   ░ ▒░
        ░      ░    ▒ ░░  ░  ░  ░  ░  ░  ░ ░ ░ ▒     ░   ░ ░
               ░    ░        ░        ░      ░ ░           ░

   ▄████▄   ▒█████   ███▄ ▄███▓ ██▓███   ██▓    ▓█████▄▄▄█████▓▓█████
  ▒██▀ ▀█  ▒██▒  ██▒▓██▒▀█▀ ██▒▓██░  ██▒▓██▒    ▓█   ▀▓  ██▒ ▓▒▓█   ▀
  ▒▓█    ▄ ▒██░  ██▒▓██    ▓██░▓██░ ██▓▒▒██░    ▒███  ▒ ▓██░ ▒░▒███
  ▒▓▓▄ ▄██▒▒██   ██░▒██    ▒██ ▒██▄█▓▒ ▒▒██░    ▒▓█  ▄░ ▓██▓ ░ ▒▓█  ▄
  ▒ ▓███▀ ░░ ████▓▒░▒██▒   ░██▒▒██▒ ░  ░░██████▒░▒████▒ ▒██▒ ░ ░▒████▒
  ░ ░▒ ▒  ░░ ▒░▒░▒░ ░ ▒░   ░  ░▒▓▒░ ░  ░░ ▒░▓  ░░░ ▒░ ░ ▒ ░░   ░░ ▒░ ░
    ░  ▒     ░ ▒ ▒░ ░  ░      ░░▒ ░     ░ ░ ▒  ░ ░ ░  ░   ░     ░ ░  ░
  ░        ░ ░ ░ ▒  ░      ░   ░░         ░ ░      ░    ░         ░
  ░ ░          ░ ░         ░                ░  ░   ░  ░           ░  ░
  ░

  ")
#+END_SRC

